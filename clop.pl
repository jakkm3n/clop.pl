#!env perl
# welcome to clop.pl!!
# by jakk h bear
# http://bitbucket.org/jakkm3n/clop.pl

use strict;
use warnings;
use utf8;

# +-----------------------=[usage]=-----------------------+
# | usage:                                                |
# |   * /clop [user]                                      |
# |     - grabs a line from clops.txt and yiffs a user    |
# |     - can be run without args to yiff a random user   |
# +-------------------------------------------------------+

my $clopfile = 'clops.txt'; # change this to the full path if the script cant find clops.txt

my $VERSION = 420;
my $DESC = 'have fun with friends';

my $CLIENT;
if (exists $INC{'Irssi.pm'}) {
	$CLIENT = 'irssi';
	my %IRSSI = (
		authors     => 'jakk',
		contact     => 'jakk on efnet',
		name        => 'clop.pl',
		description => $DESC,
		license     => 'WTFPL',
		url         => 'http://bitbucket.org/jakkm3n/clop.pl',
		changed     => 'im gay',
	);

	Irssi::command_bind('clop', \&clop);
} elsif (exists &{"weechat::register"}) {
	$CLIENT = 'weechat';
	weechat::register('clop.pl', 'jakk', $VERSION, 'WTFPL', $DESC, '', '');
	weechat::hook_command('clop', 'clop a user', '[user]',
		'clop a target or send without args to hit a random user', '', 'clop', '');
} else {
	$CLIENT = 'xchat';
	Xchat::register('clop.pl', $VERSION, $DESC);
	Xchat::hook_command('clop', \&clop);
}

sub clop {
	if ($CLIENT eq 'irssi') {
		my ($data, $server, $witem) = @_;
		if ($witem->{name} eq '') {
			print 'join a channel idiot';
			return;
		}
	} # too lazy to sanity check for weechat and xchat

	my $target = get_target(@_);
	if ($target eq '') {
		return;
	}

	if (open(CLOPS, '<', $clopfile)) {
		my @clops = <CLOPS>;
		close(CLOPS);
		my $line = $clops[rand @clops];
		$line =~ s/\$target/$target/g;

		if ($CLIENT eq 'irssi') {
			my ($data, $server, $witem) = @_;
			send_clop($line, $witem);
		} elsif ($CLIENT eq 'weechat') {
	                my ($self, $buffer, $args) = @_;
			send_clop($line, $buffer);
		} else {
		}
	} else {
		if ($CLIENT eq 'irssi') {
			Irssi::print("couldn't open $clopfile");
		} elsif ($CLIENT eq 'weechat') {
			weechat::print('', "couldn't open $clopfile");
		} else {
			Xchat::print("couldn't open $clopfile");
		}
	}
}

sub get_target { # this is mostly for getting a random target
	my $target;

	if ($CLIENT eq 'irssi') {
		my ($data, $server, $witem) = @_;

		if ($data ne '') {
			$target = $data;
		} else {
			my @users = $witem->nicks();
			my $randnum = rand @users;
			$target = $users[$randnum]->{nick};
	
			if ($target eq $server->{nick}) { # this is dumb
				splice(@users, $randnum, 1);
				$randnum = rand @users;
				$target = $users[$randnum]->{nick};
			}
		}
	} elsif ($CLIENT eq 'weechat') { # fuck weechat
                my ($self, $buffer, $args) = @_;

		if ($args ne '') {
			$target = $args;
		} else {
			my @users;
			my $infolist = weechat::infolist_get('irc_nick', '',
				weechat::buffer_get_string($buffer, 'localvar_server') . ',' . weechat::buffer_get_string($buffer, 'localvar_channel')); # lol
			while (weechat::infolist_next($infolist)) {
				my $user = weechat::infolist_string($infolist, 'name');
				if ($user ne weechat::buffer_get_string($buffer, 'localvar_nick')) {
					push(@users, $user);
				}
			}
			weechat::infolist_free($infolist);

                        $target = $users[rand (@users)];

			if ($target eq '') {
				weechat::print('', "couldn't get a random target. are u even in a channel??");
				return;
			}
		}
	} else {
		if ($_[1][1]) {
			$target = $_[1][1];
		} else {
			my @users = Xchat::get_list('users');
			my $randnum = rand @users;
			$target = $users[rand (@users)]->{nick};
			if ($target eq Xchat::get_info('nick')) {
				splice(@users, $randnum, 1);
				$target = $users[rand (@users)]->{nick};
			}
		}
	}

	$target =~ s/^\s*//;
	$target =~ s/\s*$//;
	return $target;
}

sub send_clop {
	if ($CLIENT eq 'irssi') {
		my ($line, $witem) = @_;
		$witem->command("me $line");
	} elsif ($CLIENT eq 'weechat') {
		my ($line, $buffer) = @_;
		weechat::command($buffer, "/me $line");
	} else {
		my $line = $_[0];
		Xchat::command("me $line");
		# return Xchat::EAT_XCHAT;
	}
}
